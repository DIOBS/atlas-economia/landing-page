'use strict';
const fs = require('fs');
const packageJSON = require('../package.json');
const upath = require('upath');
const sh = require('shelljs');

module.exports = function renderDependencies() {
    // Swiper JS
    var sourcePath = upath.resolve(upath.dirname(__filename), '../node_modules/swiper/swiper-bundle.min.js');
    var destPath = upath.resolve(upath.dirname(__filename), '../dist/js/');    
    sh.cp('-R', sourcePath, destPath)    

    // Jquery
    var sourcePath = upath.resolve(upath.dirname(__filename), '../node_modules/jquery/dist/jquery.min.js');
    var destPath = upath.resolve(upath.dirname(__filename), '../dist/js/');
    sh.cp('-R', sourcePath, destPath)

    // Bootstrap Icon fonts
    var sourcePath = upath.resolve(upath.dirname(__filename), '../node_modules/bootstrap-icons/font/bootstrap-icons.css');
    var destPath = upath.resolve(upath.dirname(__filename), '../dist/css/');
    sh.cp('-R', sourcePath, destPath)

    var sourcePath = upath.resolve(upath.dirname(__filename), '../node_modules/bootstrap-icons/font/fonts/');
    var destPath = upath.resolve(upath.dirname(__filename), '../dist/css/');
    sh.cp('-R', sourcePath, destPath)

    // Bootstrap
    var sourcePath = upath.resolve(upath.dirname(__filename), '../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js');
    var destPath = upath.resolve(upath.dirname(__filename), '../dist/js/');
    sh.cp('-R', sourcePath, destPath)
};
